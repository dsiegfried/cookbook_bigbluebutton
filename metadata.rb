name 'bigbluebutton'
maintainer 'Daniel Schreiber'
maintainer_email 'daniel.schreiber@hrz.tu-chemnitz.de'
license 'Apache-2.0'
description 'Installs/Configures BigBlueButton 2.3+'
version '0.1.0'
chef_version '>= 14.0'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
issues_url 'https://gitlab.com/schrd/cookbook_bigbluebutton/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
source_url 'https://gitlab.com/schrd/cookbook_bigbluebutton'

depends 'acme'
depends 'apt'
supports 'ubuntu', '~> 18.04'
