Cookbook for BigBlueButton
==========================

This cookbook installs a BigBlueButton server. It will only configure and
install BigBlueButton itself. Configuring Greenlight and scalelite is out of
scope of this cookbook. This cookbook can request webserver certificates from 
letsencrypt.

Currently the following versions are supported:

* BigBlueButton 2.3 on Ubuntu 18.04

Getting started
---------------

To use this cookbook you should implement a wrapper cookbook and add the `bigbluebutton` cookbook as dependency:

~~~ruby
# my_bbb_cookbook/metadata.rb

depends 'bigbluebutton'
~~~

To use this cookbook, you have to provide secrets for the BBB API and for etherpad. These secrets have to be provided in a chef vault. You can configure the name of the vault and item using attributes:

~~~ruby
# my_bbb_cookbook/attributes/bigbluebutton.rb
normal['bigbluebutton']['vault_name'] = 'bigbluebutton'
normal['bigbluebutton']['vault_item'] = 'secrets'
~~~

You can use the same vault for all your BBB Servers if you like. The vault is expected to have the following structure:

~~~json
{
    "bbbserver1.mydomain.org": {
        "api": "mysecretapikey",
        "etherpad": "myetherpadkey"
    },
    "bbbserver2.mydomain.org": {
        "api": "anothertapikey",
        "etherpad": "anotheretherpadkey"
    }
}
~~~

Then include the `bigbluebutton` cookbook:
~~~ruby
# my_bbb_cookbook/recipes/default.rb
include_recipe 'bigbluebutton::default'
~~~

If you want to use letsencrypt certificates, this recipe can handle this for you. All you have to do is enable it using an attribute.

~~~ruby
# my_bbb_cookbook/attributes/bigbluebutton.rb
normal['bigbluebutton']['nginx']['use_letsencrypt'] = true
~~~


Customizing your installation
-----------------------------
You can customize your BigBlueButton installation using attributes. 

### bbb-web

This cookbook will render your configuration to `/etc/bigbluebutton/bbb-web.properties`. Example:

~~~ruby
# increase upload size to 60 MB
normal['bigbluebutton']['bbb_web']['maxFileSizeUpload'] = 60000000 
normal['bigbluebutton']['bbb_web']['defaultDialAccessNumber'] = '+123-456-7890-1234'
~~~

The `securitySalt` parameter is merged in automatically. It is set from the
vault by default as described above. The `bigbluebutton.web.serverURL`
parameter is set to the FQDN by default.

### bbb-html5

Configuration is rendered to `/etc/bigbluebutton/bbb-html5.yml`. The attribute tree below `node['bigbluebutton']['bbb_html5']` is rendered as YAML file. To disable polls, you would configure
~~~ruby
normal['bigbluebutton']['bbb_html5']['public']['poll']['enabled'] = false
~~~

### Telephone dial in

There are two places that you have to configure. One is bbb-web, the other Freeswitch:

~~~ruby
normal['bigbluebutton']['bbb_web']['defaultDialAccessNumber'] = '+123-456-7890-1234'
# assume that your PBX sends only the last digits 1234 as destination to freeswitch:  
normal['bigbluebutton']['freeswitch']['dialin_match'] = '1234'
~~~
