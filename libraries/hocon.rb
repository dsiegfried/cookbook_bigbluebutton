module BigBlueButtonHelper
  def format_as_hocon(data, indent=0)
    keys = data.keys.sort
    return keys.map do |key|
      value = data[key]
      if value.is_a?(Hash)
        "#{" " * indent}#{key} {\n#{format_as_hocon(value, indent+2)}\n#{" " * indent}}"
      elsif value.is_a?(String)
        "#{" " * indent}#{key} = \"#{value}\""
      else
        "#{" " * indent}#{key} = #{value}"
      end
    end.join("\n")
  end
end
