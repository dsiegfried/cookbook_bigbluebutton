module BigBlueButtonJavaHelper
  def native2ascii(text)
    text.chars.map do |c|
      bs = c.bytes
      if bs.size == 1
        c
      else
        '\u' + c.encode("UTF-16BE").bytes.map{|b| b.to_s(16).rjust(2, '0')}.join
      end
    end.join
  end
end
