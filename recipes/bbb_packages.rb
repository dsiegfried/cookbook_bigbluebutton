user 'bigbluebutton' do
  system true
  shell '/bin/false'
end

user 'meteor' do
  system true
  home '/usr/share/meteor'
end

user 'etherpad' do
  system true
  home '/usr/share/etherpad-lite'
  shell '/bin/false'
end

directory '/etc/bigbluebutton' do
  owner 'bigbluebutton'
  # this needs to be readable to anyone, so meteor can enter this directory
  mode '0755'
end

template '/etc/bigbluebutton/bbb-apps-akka.conf' do
  owner 'bigbluebutton'
  mode '0600'
  helpers BigBlueButtonHelper
  variables 'apps_akka': node['bigbluebutton']['apps_akka']
  sensitive true
end

template '/etc/bigbluebutton/bbb-fsesl-akka.conf' do
  owner 'bigbluebutton'
  mode '0600'
  helpers BigBlueButtonHelper
  variables 'fsesl_akka': node['bigbluebutton']['fsesl_akka']
  sensitive true
end

template '/etc/bigbluebutton/bbb-web.properties' do
  owner 'bigbluebutton'
  mode '0600'
  helpers BigBlueButtonJavaHelper
  variables 'bbb_web': node['bigbluebutton']['bbb_web']
  sensitive true
end

template '/etc/bigbluebutton/bbb-html5.yml' do
  owner 'meteor'
  mode '0600'
  source 'bbb-html5.yml.erb'
  variables 'bbb_html5': YAML.dump(node['bigbluebutton']['bbb_html5'].to_hash)
  sensitive true
end

directory '/usr/share/etherpad-lite' do
  owner 'etherpad'
end

file '/usr/share/etherpad-lite/APIKEY.txt' do
  content node['bigbluebutton']['bbb_html5']['private']['etherpad']['apikey']
  mode '0400'
  owner 'etherpad'
  sensitive true
end

directory '/etc/bigbluebutton/bbb-webrtc-sfu' do
  owner 'bigbluebutton'
end

template '/etc/bigbluebutton/bbb-webrtc-sfu/production.yml' do
  owner 'bigbluebutton'
  mode '0600'
  source 'bbb-webrtc-sfu.yml.erb'
  variables 'bbb_webrtc_sfu': YAML.dump(node['bigbluebutton']['bbb_webrtc_sfu'].to_hash)
  sensitive true
end

# this may take a long time. It is pulling all the dependencies and will create
# the libreoffice docker container
package 'bigbluebutton' do
  timeout 30 * 60
end

package 'bbb-html5'

# TODO this will be in /etc starting from beta2
# for performance reasons there should be exactly one turn server only
template '/usr/share/bbb-web/WEB-INF/classes/spring/turn-stun-servers.xml' do
  owner 'bigbluebutton'
  mode '0600'
  sensitive true
  variables turnserver: node['bigbluebutton']['turnserver'],
    turnsecret: node['bigbluebutton']['turnsecret'],
    stunserver: node['bigbluebutton']['stunserver']
  only_if { (node['bigbluebutton']['turnserver'] and node['bigbluebutton']['turnsecret']) or node['bigbluebutton']['stunserver'] }
end

# enable and start services
%w(bbb-web bbb-webrtc-sfu bbb-html5 bbb-apps-akka bbb-fsesl-akka etherpad freeswitch kurento-media-server).each do |s|
  service s do
    action [:enable, :start]
  end
end
