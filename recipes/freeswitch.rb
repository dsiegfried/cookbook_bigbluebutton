user 'freeswitch' do
  system true
  shell '/bin/bash'
  home '/opt/freeswitch'
end

directory '/opt/freeswitch/etc/freeswitch' do
  recursive true
end
directory '/opt/freeswitch/etc/freeswitch/autoload_configs' do
  recursive true
end
directory '/opt/freeswitch/etc/freeswitch/dialplan/default' do
  recursive true
end
directory '/opt/freeswitch/etc/freeswitch/dialplan/public' do
  recursive true
end

template '/opt/freeswitch/etc/freeswitch/vars.xml' do
  source 'freeswitch/vars.xml.erb'
  mode '0640'
  owner 'freeswitch'
  group 'daemon'
  variables ip_address: node['bigbluebutton']['freeswitch']['host_ip'],
    ipv6_address: node['bigbluebutton']['freeswitch']['host_ipv6'],
    default_password: node['bigbluebutton']['freeswitch']['default_password']
end

# improve audio quality
%w(conference.conf.xml opus.conf.xml ).each do |f|
  cookbook_file "/opt/freeswitch/etc/freeswitch/autoload_configs/#{f}" do
    source "freeswitch/#{f}"
    mode '0640'
    owner 'freeswitch'
    group 'daemon'
  end
end
cookbook_file '/opt/freeswitch/etc/freeswitch/dialplan/default/bbb_conference.xml' do
  source 'freeswitch/bbb_conference.xml'
  mode '0640'
  owner 'freeswitch'
  group 'daemon'
end

phonenumber = node['bigbluebutton']['freeswitch']['dialin_match']
template '/opt/freeswitch/etc/freeswitch/dialplan/public/my_provider.xml' do
  action((phonenumber ? :create : :delete))
  source 'freeswitch/my_provider.xml.erb'
  mode '0640'
  owner 'freeswitch'
  group 'daemon'
  variables phonenumber: phonenumber,
    voice_bridge_digits: node['bigbluebutton']['freeswitch']['voice_bridge_digits'],
    voice_bridge_max_digits: node['bigbluebutton']['freeswitch']['voice_bridge_max_digits'],
    ivr_enter_pin: node['bigbluebutton']['freeswitch']['ivr_enter_pin'],
    ivr_invalid_pin: node['bigbluebutton']['freeswitch']['ivr_invalid_pin']
end

template '/opt/freeswitch/etc/freeswitch/autoload_configs/event_socket.conf.xml' do
  source 'freeswitch/event_socket.conf.xml.erb'
  owner 'freeswitch'
  group 'daemon'
  variables esl_password: node['bigbluebutton']['freeswitch']['esl_password']
end
