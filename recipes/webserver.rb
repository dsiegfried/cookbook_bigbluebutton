servername = node['fqdn']

package 'nginx'

directory "/etc/letsencrypt/live/#{servername}" do
  recursive true
  owner 'www-data'
  group 'www-data'
end

execute 'generate DH parameters file at /etc/letsencrypt/ssl-dhparams.pem' do
  command(lazy { 'openssl dhparam -out /etc/letsencrypt/ssl-dhparams.pem 2048' })
  action :run
  not_if { ::File.exist?('/etc/letsencrypt/ssl-dhparams.pem') }
end

acme_selfsigned servername do
  crt "/etc/letsencrypt/live/#{servername}/fullchain.pem"
  key "/etc/letsencrypt/live/#{servername}/privkey.pem"
  owner 'www-data'
  group 'www-data'
  notifies :restart, 'service[nginx]', :immediately
end

template '/etc/nginx/sites-available/bigbluebutton' do
  source 'nginx-bigbluebutton.erb'
  variables servername: servername,
    redirect_root_url: node['bigbluebutton']['nginx']['redirect_root_url']
end

service 'nginx' do
  action [:enable, :start]
end

acme_certificate servername do
  crt "/etc/letsencrypt/live/#{servername}/fullchain.pem"
  key "/etc/letsencrypt/live/#{servername}/privkey.pem"
  owner 'www-data'
  group 'www-data'
  wwwroot '/var/www/html/'
  notifies :restart, 'service[nginx]', :immediately
  only_if { node['bigbluebutton']['nginx']['use_letsencrypt'] }
  not_if { node['run_under_test'] }
end
