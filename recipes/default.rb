#
# Cookbook:: bigbluebutton
# Recipe:: default
#
# Copyright:: 2021, Daniel Schreiber

include_recipe 'acme::default'
include_recipe 'bigbluebutton::attributes'
include_recipe 'bigbluebutton::repositories'
include_recipe 'bigbluebutton::dependencies'
include_recipe 'bigbluebutton::webserver'
include_recipe 'bigbluebutton::freeswitch'
include_recipe 'bigbluebutton::bbb_packages'
include_recipe 'bigbluebutton::recording'
include_recipe 'bigbluebutton::clean_secrets'
