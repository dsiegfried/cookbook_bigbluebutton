template '/usr/local/bigbluebutton/core/scripts/bigbluebutton.yml' do
  source 'recording/bigbluebutton.yml.erb'
  owner 'bigbluebutton'
  group 'bigbluebutton'
  mode '0600'
  variables recording: YAML.dump(node['bigbluebutton']['recording'].to_hash)
end

