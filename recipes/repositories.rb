# for yq tool
apt_repository 'rmescandon-ubuntu-yq-bionic' do
  uri 'ppa:rmescandon/yq'
  key 'CC86BB64'
end

apt_repository 'libreoffice-ubuntu-ppa-bionic' do
  uri 'ppa:libreoffice/ppa'
  key '1378B444'
end

apt_repository 'bigbluebutton-ubuntu-support-bionic' do
  uri 'ppa:bigbluebutton/support'
  key 'E95B94BC'
end

apt_repository 'kurento' do
  uri 'http://ubuntu.openvidu.io/6.16.0'
  key '5AFA7A83'
  components ['kms6']
end

apt_repository 'nodesource' do
  uri 'https://deb.nodesource.com/node_12.x'
  key 'https://deb.nodesource.com/gpgkey/nodesource.gpg.key'
  components ['main']
end

apt_repository 'mongodb-org-4.2' do
  uri 'https://repo.mongodb.org/apt/ubuntu'
  distribution 'bionic/mongodb-org/4.2'
  components ['multiverse']
  key 'https://www.mongodb.org/static/pgp/server-4.2.asc'
end

apt_repository 'docker-ce' do
  uri 'https://download.docker.com/linux/ubuntu'
  key 'https://download.docker.com/linux/ubuntu/gpg'
  components ['stable']
end

apt_repository 'bigbluebutton' do
  uri 'https://ubuntu.bigbluebutton.org/bionic-230'
  key 'https://ubuntu.bigbluebutton.org/repo/bigbluebutton.asc'
  distribution 'bigbluebutton-bionic'
  components ['main']
end
